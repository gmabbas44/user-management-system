<?php

    use MyApp\Note\Note;
    include_once realpath($_SERVER['DOCUMENT_ROOT'].'/user-management-system/bootstrap.php');

    if (!isset($_POST['info_id']) && $_POST['info_id'] == null) header ('location: '.WEB_ROOT.'home.php');

    include_once (MODEL.'session.php');

    $note = new Note();
    $noteDetail = $note->viewThisNote($_POST['info_id'], $cUserId);
    

?>
<div class="card border-primary">
<div class="card-header bg-primary">
    <h2 class="float-left text-white">View note</h2>
    <button type="button" class="btn btn-light float-right back"><i class="fas fa-arrow-circle-left"></i> Back</button>
</div>

<div class="card-body">
    <table class="table table-bordered table-sm">
        <tr>
            <th width="15%">Note Author</td>
            <td width="20%"><?= $noteDetail->full_name; ?></td>
            <th width="20%">Note Author Email</td>
            <td><?= $noteDetail->email; ?></td>
        </tr>
        <tr>
            <th>Created at</td>
            <td><?= date( 'M d, Y',strtotime( $noteDetail->created_at )); ?></td>
            <th>Update at</td>
            <td><?= date( 'M d, Y',strtotime( $noteDetail->update_at)); ?></td>
        </tr>
        <tr>
            <th>Note Titlw</td>
            <td><?= $noteDetail->note_title; ?></td>
            <th>Note</td>
            <td><?= $noteDetail->note; ?></td>
        </tr>
    </table>
</div>  
</div>
