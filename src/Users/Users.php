<?php

namespace MyApp\Users;
use MyApp\Db\Db;
use \PDO;

class Users{
    public $conn;
    const EMAIL = "nikil754@gmail.com";
    const PASSWORD = "000000000";

    public function __construct()
    {
        $this->conn = Db::connection();
    }

    public function add($firstName, $lastName, $email, $password)
    {
        $sql = "INSERT INTO users(first_name, last_name, email, password ) VALUES(:firstName, :lastName, :email, :password)";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['firstName'=> $firstName, 'lastName'=> $lastName, 'email' => $email, 'password'=> $password]);
        return true;
    }

    public function user_exist($email)
    {
       $sql = "SELECT email FROM users WHERE email = :email";
       $stmt = $this->conn->prepare($sql);
       $stmt->execute(['email'=> $email]);
       $result = $stmt->fetch(PDO::FETCH_ASSOC);
       return $result;
    }
    public function login($email)
    {
        $sql = "SELECT email, password FROM users WHERE email = :email AND is_deleted = 0";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['email' => $email]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    public function loginUser($email)
    {
        $sql = "SELECT * FROM users WHERE email = :email AND is_deleted = 0 ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['email' => $email]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    public function setToken($token, $email)
    {
        $sql = "UPDATE users SET token = :token, token_expire = DATE_ADD(NOW(), INTERVAL 10 MINUTE) WHERE email = :email ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['token' => $token, 'email' => $email]);
        return true;
    }
    // get request form user for change password
    public function resetPassRequest($email, $token)
    {
        $sql = "SELECT user_id FROM users WHERE email = :email AND token = :token AND token != '' AND token_expire > now() AND is_deleted = 0 ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['email' => $email, 'token' => $token ]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    public function updatePass($email, $password)
    {
        $sql = "UPDATE users SET token = '', password = :pass WHERE email = :email AND is_deleted = 0 ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['email' => $email, 'pass' => $password ]);
        return true;
    }
    public function cUserImage($user_gender, $user_photo)
    {
        if ( !strtolower( $user_gender )  && !$user_photo ) {
            $img = 'avatar.png';
        } elseif ( strtolower( $user_gender ) == 'male' && $user_photo == null ) {
            $img = 'male.png';
        } elseif ( strtolower( $user_gender ) == 'female' && $user_photo == null ) {
            $img = 'female.jpg';
        } else {
            $img = $user_photo;
        }
        return $img;
    }
    public function updateUser($id, $firstName, $lastName, $phone, $gender, $dateOfBirth, $img )
    {
        $sql = " UPDATE users SET first_name = :fname, last_name = :lname, phone = :phone, gender = :gender, dateOfBirth = :dob, photo = :img WHERE user_id = :id AND is_deleted = 0 ";
        $stmt = $this->conn->prepare( $sql );
        $stmt->execute( ['fname' => $firstName, 'lname' => $lastName, 'phone' => $phone, 'gender' => $gender, 'dob' => $dateOfBirth, 'img' => $img, 'id' => $id] );
        return true;
    }
    public function matchPass( $user_id, $cPass )
    {
       $sql = " SELECT password FROM users WHERE user_id = :user_id ";
       $stmt = $this->conn->prepare($sql);
       $stmt->execute(['user_id' => $user_id]);
       $res = $stmt->fetch(PDO::FETCH_ASSOC);
       if( $res['password'] == $cPass ) {
            return true;
       } else {
            return false;
       }
    }
    public function changePass($user_id, $newPassword)
    {
        $sql = " UPDATE users SET password = :newPassword WHERE user_id = :user_id ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['newPassword' => $newPassword, 'user_id' => $user_id]);
        return true;
    }

    public function userOnline($s_id)
    {
        $time = time();
        $sql = " SELECT session FROM user_online WHERE session = :session ;" ;
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['session' => $s_id]);
        $rowCount = $stmt->rowCount();

        if ($rowCount == null ) {
            $sql = " INSERT INTO user_online(session, session_time) VALUES(:session, :time) ; ";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute(['session' => $s_id, 'time' => $time]);
        } else {
           $sql = " UPDATE user_online SET session_time = :time WHERE session = :s_id ; ";
           $stmt = $this->conn->prepare($sql);
           $stmt->execute(['s_id' => $s_id, 'time' => $time]);
        }
        
    }
}

?>