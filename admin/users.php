<?php

use MyApp\Users\Users;

include_once realpath($_SERVER['DOCUMENT_ROOT'].'/user-management-system/bootstrap.php');

include (ADMIN.'model/session.php');

$users = $admin->allUser(0);

$userClass = new Users;

?>

<!DOCTYPE html>
<html lang="en">

<head>
<!-- css directory -->
<?php require_once(ROOT_DIR."/_inc/css.php");?>
<!-- Custom styles for this template-->
<link href="<?= CSS ;?>sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

<?php require_once( ADMIN.'admin_inc/header.php' ); ?>


<div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="card">
			<div class="card-header bg-primary text-white ">
				<h4 class="text-center">All Users</h4>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-sm table-bordered table-striped border-light">
						<thead class="text-center">
							<th>Sl</th>
							<th>Photo</th>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Gender</th>
							<th>Is verified</th>
							<th>Action</th>
						</thead>
						<tbody id="showusers">
						
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="userInfo" tabindex="-1" role="dialog" aria-labelledby="userInfo" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="userInfo">User Information</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <!-- ajax request -->
		<div id="data"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php require_once( ADMIN.'admin_inc/footer.php' ); ?>
  <!-- Bootstrap core JavaScript-->
<?php require_once(ROOT_DIR."/_inc/js.php");?>

<script>
$(document).ready( function () {

	showUsers();
	function showUsers() {
		$.ajax({
			url		: 'model/action.php',
			type	: 'POST',
			data	: { showUser : 'showUser' },
			success : function (reaponse) {
				$('#showusers').html(reaponse);
				$('table').DataTable({
					order : [0, 'desc']
				});
			}
		});
	}
	
	$('body').on('click', '.info-btn', function(e) {
		e.preventDefault();
		var id = $(this).attr('id');
		$.ajax({
			url 	: 'model/action.php',
			type 	: 'POST',
			data 	: { user_id : id },
			success : function (response) {
				console.log(response);
				$('#data').html(response);
			}
		});
	});

	$('body').on('click', '.delete-btn', function(e) {
		e.preventDefault();
		var del_id = $(this).attr('id');
		Swal.fire({
			title				: 'Are you sure?',
			text				: "You won't be able to revert this!",
			icon 				: 'warning',
			showCancelButton 	: true,
			confirmButtonColor 	: '#3085d6',
			confirmButtonText   : 'Yes, delete it!',
			cancelButtonColor	: '#d33'
		}).then( ( result ) => {
			if ( result.value ) {
				$.ajax({
					url		: 'model/action.php',
					type	: 'POST',
					data	: { del_user_id : del_id },
					success	: function (response) {
						Swal.fire({
							title   : 'Deleted!',
							text    : 'User has been deleted!',
							icon    : 'success'
						});
						showUsers();
					}
				});
			} else {
				Swal.fire ({
					title : 'Cancelled',
					text : 'Your imaginary file is safe',
					icon : 'error'
				});
			}
		});
	});
    
});
    
</script>


</body>

</html>
