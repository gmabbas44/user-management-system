<?php

include_once realpath($_SERVER['DOCUMENT_ROOT'].'/user-management-system/bootstrap.php');

include (ADMIN.'model/session.php');

?>

<!DOCTYPE html>
<html lang="en">

<head>
<!-- css directory -->
<?php require_once(ROOT_DIR."/_inc/css.php");?>
<!-- Custom styles for this template-->
<link href="<?= CSS ;?>sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

<?php require_once( ADMIN.'admin_inc/header.php' ); ?>


<div class="card">
	<div class="card-header bg-primary">
		<h4 class="text-white text-center">Delete Users</h4>
	</div>
	<div class="card-body">
	<div class="table-responsive">
		<table class="table table-sm table-bordered table-striped border-light">
			<thead class="text-center">
				<th>Sl</th>
				<th>Photo</th>
				<th>Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Gender</th>
				<th>Is verified</th>
				<th>Action</th>
			</thead>
			<tbody id="showDelUser">

			</tbody>
		</table>
	</div>
</div>


<?php require_once( ADMIN.'admin_inc/footer.php' ); ?>
  <!-- Bootstrap core JavaScript-->
<?php require_once(ROOT_DIR."/_inc/js.php");?>

<script>
$(document).ready( function () {

	showDeleteUser();
	function showDeleteUser() {
		$.ajax({
			url		: 'model/action.php',
			type	: 'POST',
			data	: { showDelUser : 'showDelUser' },
			success : function (reaponse) {
				$('#showDelUser').html(reaponse);
				$('table').DataTable({
					order : [0, 'desc']
				});

			}
		});
	}

	$('body').on('click', '.restore-btn', function (e) {
		e.preventDefault();
		var id = $(this).attr('id');
		Swal.fire({
			title				: 'Are you sure?',
			text				: "You are able to revert this!",
			icon 				: 'warning',
			showCancelButton 	: true,
			confirmButtonColor 	: '#3085d6',
			confirmButtonText   : 'Yes, restore it!',
			cancelButtonColor	: '#d33'
		}).then( ( result ) => {
			if ( result.value ) {
				$.ajax({
					url		: 'model/action.php',
					type	: 'POST',
					data	: { restore_id : id },
					success	: function (response) {
						Swal.fire({
							title   : 'Restore!',
							text    : 'User has been Restore!',
							icon    : 'success'
						});
						showDeleteUser();
					}
				});
			} else {
				Swal.fire ({
					title : 'Cancelled',
					text : 'Your imaginary file is safe',
					icon : 'error'
				});
			}
		});
	});

	$('body').on('click', '.delete-btn', function (e) {
		e.preventDefault();
		var del_id = $(this).attr('id');
		Swal.fire({
			title				: 'Are you sure?',
			text				: "You won't be able to revert this!",
			icon 				: 'warning',
			showCancelButton 	: true,
			confirmButtonColor 	: '#3085d6',
			confirmButtonText   : 'Yes, Delete it!',
			cancelButtonColor	: '#d33'
		}).then( ( result ) => {
			if ( result.value ) {
				$.ajax({
					url		: 'model/action.php',
					type	: 'POST',
					data	: { del_id : del_id },
					success	: function (response) {
						Swal.fire({
							title   : 'Deleted!',
							text    : 'User has been Deleted!',
							icon    : 'success'
						});
						showDeleteUser();
					}
				});
			} else {
				Swal.fire ({
					title : 'Cancelled',
					text : 'Your imaginary file is safe',
					icon : 'error'
				});
			}
		});
		
	});
    
});
    
</script>


</body>

</html>
