<?php  include_once ('header.php'); ?>

<?php
    if (isset($_GET['noti_id']) && !empty($_GET['noti_id'])) {
        $id =  $_GET['noti_id'];
    } else {
        $id = '';
    }

?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div id="main-content">
                <!-- include ajax request -->
            </div>
        </div>
    </div>
</div>
<!-- js directory -->
<?php require_once(ROOT_DIR."/_inc/js.php");?>
<?php require_once('ajax.php'); ?>

<script>
    $(document).ready(function(){
       
        setInterval(function(){
            notification();
        },1000);
       function notification(){
           $.ajax({
               url  : 'model/process.php',
               type : 'POST',
               data : {
                   action   : 'notification',
                   id       : '<?= $id ?>'
                   },
               success : function(reaponse) {
                   $('#main-content').html(reaponse);
                //console.log(reaponse);
               }
           });
       }
    

    });
</script>
</body>
</html> 