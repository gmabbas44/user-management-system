<?php

include_once realpath($_SERVER['DOCUMENT_ROOT'].'/user-management-system/bootstrap.php');

include (ADMIN.'model/session.php');

?>

<!DOCTYPE html>
<html lang="en">

<head>
<!-- css directory -->
<?php require_once(ROOT_DIR."/_inc/css.php");?>
<!-- Custom styles for this template-->
<link href="<?= CSS ;?>sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

<?php require_once( ADMIN.'admin_inc/header.php' ); ?>

<!-- Content Row -->
<div class="row mb-2">

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-md-3 mb-2">
    <div class="card border-primary shadow h-100 py-2 text-center">
      <div class="card-header m-1">
        <div class="text-sm font-weight-bold text-primary mb-1">Total Users</div>
      </div>
      <div class="card-body">
        <h2 id='userCount'>
            <!-- ajax request -->
            <img width="30" src="<?= IMG . 'loader.gif'?>" alt="preloader">
        </h2>
      </div>
    </div>
  </div>

  <div class="col-md-3 mb-2">
    <div class="card border-info shadow h-100 py-2 text-center">
      <div class="card-header m-1">
        <div class="text-sm font-weight-bold text-info mb-1">Verified Users</div>
      </div>
      <div class="card-body">
        <h2><?= $admin->is_verified(1) ; ?></h2>
      </div>
    </div>
  </div>

  <div class="col-md-3 mb-2">
    <div class="card border-danger shadow h-100 py-2 text-center">
      <div class="card-header m-1">
        <div class="text-sm font-weight-bold text-danger mb-1">Unverified Users</div>
      </div>
      <div class="card-body">
        <h2><?= $admin->is_verified(0) ; ?></h2>
      </div>
    </div>
  </div>

  <div class="col-md-3 mb-2">
    <div class="card border-success shadow h-100 py-2 text-center">
      <div class="card-header m-1">
        <div class="text-sm font-weight-bold text-success mb-1">User Online</div>
      </div>
      <div class="card-body">
        <h2 id="active_user">
          <!-- ajax request -->
          <img width="30" src="<?= IMG . 'loader.gif'?>" alt="preloader">
        </h2>
      </div>
    </div>
  </div>

</div>

<div class="row">
  <div class="col-md-4 mb-2">
    <div class="card border-left-success shadow h-100 py-2">
      <div class="card-body">
        <div class="row align-items-center">
          <div class="col mr-2">
            <div class="text-sm font-weight-bold text-success mb-1">Total Note</div>
          </div>
          <div class="col-auto">
            <span id="totalNote" class="display-4">
               <!-- ajax request -->
               <img width="30" src="<?= IMG . 'loader.gif'?>" alt="preloader">
              </span>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-4 mb-2">
    <div class="card border-left-warning shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-sm font-weight-bold text-warning mb-1">Total Feedback</div>
          </div>
          <div class="col-auto">
            <span id="feedback" class="display-4">
              <!-- ajax request -->
              <img width="30" src="<?= IMG . 'loader.gif'?>" alt="preloader">
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-4 mb-2">
    <div class="card border-left-info shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-sm font-weight-bold text-info mb-1">Total Notification</div>
          </div>
          <div class="col-auto">
            <span id="notification" class="display-4">
              <!-- ajax request -->
              <img width="30" src="<?= IMG . 'loader.gif'?>" alt="preloader">
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
<div class="row">

  <div class="col-md-6 col-lg-6 mb-2">
    <div class="card border-success p-1 text-center">
      <div class="card-header m-1">
      <div class="text-sm font-weight-bold text-success mb-1">Male/Female User's percentage</div>
      </div>
      <div class="card-body">
        <div id="chart-1" class="text-center"></div>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-lg-6 mb-2">
    <div class="card border-primary p-1 text-center">
      <div class="card-header m-1">
      <div class="text-sm font-weight-bold text-primary mb-1">Verified/Unverified User's percentage</div>
      </div>
      <div class="card-body">
        <div id="chart-2" class="text-center"></div>
      </div>
    </div>
  </div>

</div>


<?php require_once( ADMIN.'admin_inc/footer.php' ); ?>
  <!-- Bootstrap core JavaScript-->
<?php require_once(ROOT_DIR."/_inc/js.php");?>
<script type="text/javascript" src="<?= JS."google-chart.js" ;?>"></script>

<script>
$(document).ready( function () {

    setInterval( function() {
        count('#userCount', 'users'); // total user count
        count('#totalNote', 'note'); // count total note
        count('#feedback', 'feedback'); // count total feedback
        count('#notification', 'notification'); // count total notification
        userOnline(); // active user online
    },2000);

    function count(id, tableName) {
        $.post('model/action.php',
        { tableName : tableName },
        function(data, status){
            $(id).text(data);
        });
    }
    
    function userOnline() {
        $.ajax({
            url     : 'model/action.php',
            type    : 'POST',
            data    : { action : 'userOnline'},
            success : function(response) {
                $('#active_user').text(response);
            }
        });
    }
    
});
    
</script>

<script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(chartOne);
      function chartOne() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          <?php
            $gender = $admin->genderPer();
            foreach ( $gender as $row) {
              echo "['".ucfirst($row['gender'])."',".$row['number']."],";
            }
          ?>
          
        ]);

        var options = {
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart-1'));
        chart.draw(data, options);
      }


      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            <?php
                $verifyPer = $admin->verifyPer();
                foreach ($verifyPer as $row) {
                    if ($row['is_verified'] == 1) {
                        echo "['Verified', ".$row['number']."],";
                    } else {
                        echo "['Unverified', ".$row['number']."],";
                    }
                }
            ?>
        ]);

        var options = {
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart-2'));
        chart.draw(data, options);
      }
      $(window).resize(function(){
        chartOne();
        drawChart();
      });
    </script>

</body>

</html>
