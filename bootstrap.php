<?php 
    session_start();
    include_once('vendor/autoload.php');
    include_once('mail/vendor/autoload.php');
    

    define('DOCUMENT_ROOT',$_SERVER['DOCUMENT_ROOT']);
    define('ROOT_DIR', DOCUMENT_ROOT.'/user-management-system');
    define('WEB_ROOT','http://localhost/user-management-system/');
    define('CSS',WEB_ROOT.'css/');
    define('JS',WEB_ROOT.'js/');
    define('IMG',WEB_ROOT.'img/');
    define('FONTAWESOM',WEB_ROOT.'fontawesome-free/css/');
    define("MODEL", ROOT_DIR."/model/");
    define("_INC", ROOT_DIR."/_inc/");
    define('USER_IMG',WEB_ROOT.'img/user-img/');
    define('UPLOAD_IMG',ROOT_DIR.'/img/user-img/');
    define('ADMIN', ROOT_DIR.'/admin/');
    define('ADMIN_WEB_ROOT','http://localhost/user-management-system/admin/')
 

?>