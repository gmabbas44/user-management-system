<?php

use MyApp\Users\Users;
use MyApp\Utility\Helper;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

include_once realpath($_SERVER['DOCUMENT_ROOT'].'/user-management-system/bootstrap.php');
// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

$users = new Users();

// Register ajax request
if (isset($_POST['action']) && $_POST['action'] == "register") {
    $firstName  = Helper::escapeString($_POST['reg_first_name']);
    $lastName   = Helper::escapeString($_POST['reg_last_name']);
    $email      = Helper::escapeString($_POST['reg_email']);
    $password   = Helper::escapeString($_POST['reg_pass']);
    
    if ($users->user_exist($email)) {
        echo Helper::messages('warning','This user already exist!...');
    } else {
        if ($users->add($firstName, $lastName, $email, $password)) {
            echo "reg_success";
            $_SESSION['user_email'] = $email;
        } else {
            echo Helper::messages('danger','Something problem. Try again');
        }
    }
}

//login ajax request
if (isset($_POST['action']) && $_POST['action'] == 'login') {
    $logEmail   = Helper::escapeString($_POST['log_email']);
    $logPass    = Helper::escapeString($_POST['log_pass']);
    $logUser    = $users->login($logEmail);

    if ($logUser['email'] != null) {
        if ($logUser['password'] == $logPass) {
            if (!empty($_POST['remember'])) {
                setcookie('email',$logEmail, time()+(30*24*60*60), '/');
                setcookie('password',$logPass, time()+(30*24*60*60), '/');
            } else {
                setcookie('email', ' ', 1, '/');
                setcookie('password', ' ', 1, '/');
            }
            echo "login";
            $_SESSION['email'] =  $logUser['email'];
        } else {
            echo Helper::messages('danger','<strong>Sorry! </strong> You password is wrong.');
        }
    }else{
        echo Helper::messages('danger','<strong>Sorry! </strong> user not found.');
    }
}

// forgot ajax request
if (isset($_POST['action']) && $_POST['action'] == 'forgot') {
    $email = Helper::escapeString($_POST['forgot-email']);
    $foundUser = $users->user_exist($email);
    
    if ($foundUser['email'] != null) {
        $token = str_shuffle(uniqid(time())).str_shuffle(uniqid());

        try {
            $users->setToken($token, $email);
            //Server settings
            $mail->SMTPDebug = SMTP::DEBUG_SERVER; 
            $mail->isSMTP();
            $mail->Host         = "smtp.gmail.com";
            $mail->SMTPAuth     = "true";
            $mail->Username     = Users::EMAIL;
            $mail->Password     = Users::PASSWORD;
            $mail->SMTPSecure   = "tls";
            $mail->SMTPSecure   = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port         = 587;

            $mail->setFrom(Users::EMAIL, "form Web");
            

            // $mail->isHTML(true);
            $mail->Subject = "Reset Password";
            $mail->Body = '<h3>Click the below link to reset password<br><a href="http://localhost/user-management-system/reset-password.php?email='.$email.'&token='.$token.'">http://localhost/user-management-system/reset-passowrd.php?email='.$email.'&token='.$token.'</a></h3>';
            $mail->addAddress($email);
            
            if ($mail->Send()) {
                echo Helper::messages('success','<strong>Please! </strong> Checked your email and reset you password.');
            }else {
                echo Helper::messages('warning','<strong>Opps!! </strong> Something wrong.');
            }
            
            $mail->smtpClose();
        } catch (Exception $e) {
            $e->getMessage();
            // echo Helper::messages('warning','<strong>Oops! </strong> Something wrong.');
        }
    } else {
        echo Helper::messages('warning','<strong>Sorry! </strong> This user is not found.');
    }
}
if( isset( $_FILES['user_img'] ) ) {
    $id         = Helper::escapeString( $_POST['cUser_id'] );
    $firstName  = Helper::escapeString( $_POST['firstName'] );
    $lastName   = Helper::escapeString( $_POST['lastName'] );
    $phone      = Helper::escapeString( $_POST['phone'] );
    $gender     = Helper::escapeString( $_POST['gender'] );
    $dateOfBirth= Helper::escapeString( $_POST['dateOfBirth'] );
    $imgOldName = Helper::escapeString( $_POST['oldImg'] );
    $newImgName = $_FILES['user_img']['name'];
    $newImgTmp  = $_FILES['user_img']['tmp_name'];
    
    if ( $newImgName == null ) {
        $img = $imgOldName;
    } else {
        $imgExt = pathinfo( $newImgName, PATHINFO_EXTENSION );
        $img    = 'userId_'.$id.'_'.uniqid().'.'.$imgExt;
        move_uploaded_file( $newImgTmp, UPLOAD_IMG.$img );
    }
    $res = $users->updateUser( $id, $firstName, $lastName, $phone, $gender, $dateOfBirth, $img );
    if ( $res ) {
        echo 'success';
    } else {
        echo 'wrong';
    }
}

if ( isset( $_POST['cPassword'] ) && !empty( $_POST['cPassword'] ) ) {
    $user_id = $_POST['user_id'];
    $cPass = Helper::escapeString( $_POST['cPassword'] );

    $res = $users->matchPass($user_id,$cPass);
    if ( $res == true){
        echo '<span class="text-success font-weight-bold">Password matched!</span>';
    } else {
        echo '<span class="text-danger font-weight-bold">Password does not matched!</span>';
    }

}
if ( isset( $_POST['action'] ) && $_POST['action'] == 'changePass' ) {
    $user_id = $_POST['user_id'];
    $currentPassword = Helper::escapeString( $_POST['currentPassword'] );
    $newPassword = Helper::escapeString( $_POST['newPassword'] );
    $confirmPassword = Helper::escapeString( $_POST['confirmPassword'] );

    if ( $newPassword ==  $confirmPassword){
        if ( $users->matchPass( $user_id, $currentPassword ) ){
            $users->changePass($user_id, $newPassword);
            echo Helper::messages('warning','<strong>Wow! </strong> Password change successfully!.');
        } else {
            echo Helper::messages('warning','<strong>Sorry! </strong> Your current password is wrong!.');
        }
    } else {
        echo Helper::messages('warning','<strong>Sorry! </strong> Passwords not match!.');
    }
}

if ( isset( $_POST['action']) && $_POST['action'] == 'verified_email') {
    //echo 'Helo';
}


?>