<?php
    include_once ('header.php');
?>

<div class="container">
     <!-- main contain -->
    <div id="main-contain">
    <h3 class="text-center text-primary" style="margin-top:150px;">Loading....</h3>
        <!-- include page by ajax require_once -->
    </div>
</div>

 <!-- Add Note Modal -->
 <div class="modal fade" id="addNote" tabindex="-1" role="dialog" aria-labelledby="addNoteTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content border-primary">
            <div class="modal-header">
                <h5 class="modal-title" id="addNoteTitle">Add a New Note</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addNote-form">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="noteTitle">Title</label>
                        <input type="text" class="form-control" name="noteTitle" id="noteTitle" placeholder="Note Title" required>
                    </div>
                    <div class="form-group">
                        <label for="note">Note</label>
                        <textarea class="form-control" id="note" name="note" placeholder="Write something" rows="5" required></textarea>
                    </div>   
                </div>
                <div class="modal-footer">
                    <button type="submit" name="add" id="add-note-btn"  class="btn btn-block btn-primary">Add a New Note</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /End add note modal -->

<!-- update model -->
<div class="modal fade" id="update-box" tabindex="-1" role="dialog" aria-labelledby="update-box" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content border-primary">
            <div class="modal-header">
                <h5 class="modal-title" id="update-box">Update Note</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="update-form">
                <input type="hidden" name="update_id" id="update_id" >
                <div class="modal-body">
                    <div class="form-group">
                        <label for="updatTitle">Update Title</label>
                        <input type="text" class="form-control" name="updateTitle" id="updateTitle" placeholder="Note Title">
                    </div>
                    <div class="form-group">
                        <label for="updateNote">Update Note</label>
                        <textarea class="form-control" id="updateNote" name="updateNote" placeholder="Write something" rows="5"></textarea>
                    </div>   
                </div>
                <div class="modal-footer">
                    <button id="update-btn" type="button" class="btn btn-info btn-block">Update Note</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End update modal -->

<!-- js directory -->
<?php require_once(ROOT_DIR."/_inc/js.php");?>
<?php require_once('ajax.php'); ?>

<script>
    $(document).ready(function(){
        // show all users
        function showUser() {
            $.ajax({
                url     : "_inc/data-table.php",
                type    : "POST",
                data    : {action : 'action'},
                success : function(response) {
                    $('#main-contain').html(response);
                    $('[data-toggle="tooltip"]').tooltip();
                    $('table').DataTable({
                        order : [0, 'desc']
                    });
                }
            });
        }
        showUser();
        
        // add note
        $('#add-note-btn').click(function(e) {
            if ($('#addNote-form')[0].checkValidity()) {
                e.preventDefault();
                $('#add-note-btn').text('Please wait....');
                $.ajax({
                    url     : "model/process.php",
                    type    : "POST",
                    data    : $('#addNote-form').serialize()+"&action=addNote",
                    success : function (response) {
                        $('#add-note-btn').text('Add a New Note');
                        if (response == 'success') {
                            Swal.fire({
                                icon    : 'success',
                                title   : 'Note Added Successfully!..',
                                type    : 'success',
                                timer   : 5000
                            });
                        } else {
                            Swal.fire({
                                icon    : 'error',
                                title   : 'Oops...',
                                text    : 'Something went wrong!',
                            });
                        }
                        $('#addNote').modal('hide');
                        $('#addNote-form')[0].reset();
                        showUser();
                    } 
                });
            }
        });
        // get date for update
        $('body').on('click', '.btn-update',function(e){
            e.preventDefault();
            var id = $(this).attr('id');
            
            $.ajax({
                url     : "model/process.php",
                type    : "POST",
                data    : {edit_id : id},
                success : function (response) {
                    data = JSON.parse(response);
                    $('#update_id').val(data.note_id);
                    $('#updateTitle').val(data.note_title);
                    $('#updateNote').val(data.note);
                }
            });
            
        });
        //update note
        $('#update-btn').click(function(e){
            if ($('#update-form')[0].checkValidity()) {
                e.preventDefault();
                $('#update-btn').text('Please wait.....');
                $.ajax({
                    url     : "model/process.php",
                    type    : "POST",
                    data    : $('#update-form').serialize()+"&action=update",
                    success : function (response) {
                        $('#update-btn').text('Update Note');
                        if ( response == 'success') {
                            Swal.fire({
                                icon    : 'success',
                                title   : 'Success',
                                text    : 'Note update successfully!'
                            });
                        } else {
                            Swal.fire({
                                icon    : 'error',
                                title   : 'Oops..',
                                text    : 'Something went wrong!'
                            });
                        }
                        $('#update-box').modal('hide');
                        $('#update-form')[0].reset();
                        showUser();
                    }
                });
            }
        });
        // delete note
        $('body').on('click', '.delete-btn', function(e) {
            e.preventDefault();
            var del_id  = $(this).attr('id');
            var tr      = $(this).closest('tr');
            Swal.fire({
                title               : 'Are you sure?',
                text                : "You won't be able to revert this!",
                icon                : 'warning',
                showCancelButton    : true,
                confirmButtonColor  : '#3085d6',
                cancelButtonColor   : '#d33',
                confirmButtonText   : 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url         : 'model/process.php',
                        type        : 'POST',
                        data        : {del_id : del_id},
                        success     : function (response) {
                            tr.css('background', '#f00');
                            Swal.fire({
                                title   : 'Deleted!',
                                text    : 'Note delete successfully!',
                                icon    : 'success'
                            });
                            showUser();
                        }
                    });
                } else {
                    Swal.fire({
                        title : 'Cancelled',
                        text : 'Your imaginary file is safe',
                        icon : 'error'
                    });
                }
            });
        });
        // note view ajax request
        $('body').on('click', '.info-btn', function (e) {
            e.preventDefault();
            var info_id = $(this).attr('id');
            $.ajax({
                url     : '_inc/note-view.php',
                type    : 'POST',
                data    : { info_id : info_id },
                success : function (response) {
                    $('#main-contain').html(response);
                }
            });
        });
        
        $('body').on('click', '.back', function(e) {
            showUser();
            $('[data-toggle="tooltip"]').tooltip('hide');
        });

    });
</script>
</body>
</html> 