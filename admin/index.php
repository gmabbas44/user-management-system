<?php
	use MyApp\Admin\Admin;
	include_once realpath($_SERVER['DOCUMENT_ROOT'].'/user-management-system/bootstrap.php');
	if (isset($_SESSION['email'])) {
        header('location: '.ADMIN_WEB_ROOT.'deshboard.php');
    }
?>
<?php
	$admin = new Admin();
?>

<!DOCTYPE html>
<html lang="en">

<head>
<!-- css directory -->
<?php require_once(ROOT_DIR."/_inc/css.php");?>

  <!-- Custom styles for this template-->
  <link href="<?= CSS ;?>sb-admin-2.min.css" rel="stylesheet">
  <style>
	  html, body {
		  height: 100%;
	  }
  </style>

</head>

<body class="bg-dark">
	<div class="container h-100">
		<div class="row h-100 align-items-center justify-content-center">
			<div class="col-lg-5">
				<div class="card border-danger">
					<div class="card-header bg-danger">
						<h3 class="text-white"><i class="fas fa-user-cog"></i> Admin login panel</h3>
					</div>
					<div class="card-body">
						<form action="#" id="admin-form">
							<div id="ErrAlert"></div>
							<div class="form-group">
								<input class="form-control" type="email" name="email" id="email" placeholder="Enter your email" autofocus required>
								<small id="existAdmin"></small>
							</div>
							<div class="form-group">
								<input class="form-control" type="password" name="password" id="password" placeholder="Enter your password" required>
							</div>
							<button class="btn btn-danger btn-block" name="submit-btn" id="admin-login-btn" class="btn btn-danger">Login</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- Bootstrap core JavaScript-->
<?php require_once(ROOT_DIR."/_inc/js.php");?>
<script>
$(document).ready(function () {

	$('#admin-login-btn').click(function (e) {
		if ($('#admin-form')[0].checkValidity()) {
			e.preventDefault();
			$('#admin-login-btn').text('Please wait ...');
			$.ajax({
				url		: 'model/action.php',
				type	: 'POST',
				data	: $('#admin-form').serialize()+"&action=login",
				success	: function(response) {
					$('#admin-login-btn').text('Login');
					if (response === 'login') {
						window.location = 'deshboard.php';
					} else {
						$('#ErrAlert').html(response);
					}
				}
			});
		}
	});
	
	$('#email').keyup(function() {
		var email = $(this).val();
		$.post('model/action.php',
		{
			email : email
		}, 
		function (data,status){
			$('#existAdmin').html(data);
		});
	});

});
</script>

</body>

</html>
