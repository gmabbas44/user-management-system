<?php

include_once realpath($_SERVER['DOCUMENT_ROOT'].'/user-management-system/bootstrap.php');

include(MODEL.'session.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- css directory -->
<?php require_once(ROOT_DIR."/_inc/css.php");?>


</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand text-white" href="#"><i class="fas fa-user-circle fa-lg"></i> User Management System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link text-white" href="<?= WEB_ROOT.'home.php'; ?>" id="home"><i class="fas fa-home"></i> Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="<?= WEB_ROOT.'feedback.php'; ?>" id="feedback"><i class="far fa-comment-dots"></i> Feedback</a>
                </li>
                <li class="nav-item noti-card">
                    <a class="nav-link text-white" href="<?= WEB_ROOT.'notification.php'; ?>" id="notification"><i class="far fa-bell"></i> Notification <span id="count" class="badge badge-light"></span></a>
                    <div class="nav-card">
                        <div class="box"></div>
                        <div class="nav-card-head">
                            <h5>Notification</h5>
                        </div>
                        <div class="nav-card-body" id="showNotificatin" >
                            <!-- include ajax request -->
                        </div>
                        <div class="nav-card-foor">
                            <a href="#">See more notificaton</a>
                        </div>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-white dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-user"></i> <?= $currentUser['first_name'];?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?= WEB_ROOT.'profile.php'; ?>"><i class="fas fa-user-md"></i> Profile</a>
                        <a class="dropdown-item" href="#"><i class="fas fa-user-cog"></i> Setting</a>
                        <a class="dropdown-item" href="<?= WEB_ROOT.'logout.php'; ?>"><i class="fas fa-sign-out-alt"></i> Log Out</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row mt-2">
        <div class="col-md-12">
            <?php if ($currentUser['is_verified'] == 0):?>
            <div class="alert alert-warning" role="alert">
                Sorry! Your e-mail is not verified. Please! Chceked your E-mail and active your account,
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>