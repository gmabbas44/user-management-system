<?php

use MyApp\Admin\Admin;

if ( !isset( $_SESSION['email'] )) {
    header("location: ".ADMIN_WEB_ROOT."index.php");
    die();
}
$admin = new Admin;
$currentAdmin = $admin->currentAdmin($_SESSION['email']);
$currentAdmin['admin_id'];

?>