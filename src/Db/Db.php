<?php

namespace MyApp\Db;
use \PDO;

class Db{
	public static $dbhost = 'localhost';
	public static $db_name = 'user_info';
	public static $db_user = 'root';
	public static $db_pass = '';

	public static function connection(){

		try {
			$conn = new PDO('mysql:host='.SELF::$dbhost.';dbname='.SELF::$db_name.'',SELF::$db_user,SELF::$db_pass);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $conn;
			
		} catch (\PDOException $e) {
			echo "connection Failed!!!...".$e->getMessage();
		}

	}

}



?>