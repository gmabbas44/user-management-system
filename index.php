<?php

    include_once realpath($_SERVER['DOCUMENT_ROOT'].'/user-management-system/bootstrap.php');
    if (isset($_SESSION['email'])) {
        header('location: '.WEB_ROOT.'home.php');
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- css directory -->
<?php require_once(ROOT_DIR."/_inc/css.php");?>

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div id="login-in">
                <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-color">
                            <div class="p-5">
                                <div class="text-center ">
                                    <h2 class="h4 text-white-900 mb-4">Hello Friend..</h2>
                                    <p>To keep connect with us please create an account</p>
                                    <button id="sing-up-btn" class="btn btn-custom">Sign Up</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                </div>
                                <form class="user" method="post" id="login-form">
                                    <div id="logErrAlert"></div>
                                    <div class="form-group">
                                    <input type="email" name="log_email" class="form-control form-control-user" id="login_email" placeholder="Enter Email Address..." value="<?php if(isset($_COOKIE['email'])){ echo $_COOKIE['email']; } ?>" required>
                                    </div>
                                    <div class="form-group">
                                    <input type="password" name="log_pass" class="form-control form-control-user" id="login_pass" placeholder="Password" value="<?php if(isset($_COOKIE['password'])){ echo $_COOKIE['password']; } ?>" required>
                                    </div>
                                    <div class="form-group">
                                    <div class="custom-control custom-checkbox small">
                                        <input name="remember" type="checkbox" class="custom-control-input" id="remember" <?php if(isset($_COOKIE['email'])){ echo "checked"; } ?> >
                                        <label class="custom-control-label" for="remember">Remember Me</label>
                                    </div>
                                    </div>
                                    <button id="login" class="btn btn-primary btn-user btn-block">Login</button>
                                </form>
                                <hr>
                                <button id="forget-btn" class="btn btn-primary btn-user btn-block">Forgot Password?</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="sing-up">
                <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                    <div class="row">
                        
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                                </div>
                                <form class="user" method="POST" id="register-form">
                                    <div id="regErrAlert"></div>
                                    <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" name="reg_first_name" class="form-control form-control-user" id="reg_first_name" placeholder="First Name" required>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" name="reg_last_name" class="form-control form-control-user" id="reg_last_name" placeholder="Last Name" required>
                                    </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="reg_email" class="form-control form-control-user" id="reg_email" placeholder="Email Address" required>
                                        </div>
                                        <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <input type="password" name="reg_pass" class="form-control form-control-user" id="reg_pass" placeholder="Password" required>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="password" name="reg_confirm_pass" class="form-control form-control-user" id="reg_confirm_pass" placeholder="Repeat Password" required>
                                        </div>
                                        <div id="error-msg" class="text-danger"></div>
                                    </div>
                                    <button type="submit" id="register-btn" class="btn btn-primary btn-user btn-block">Register Account</button>
                                </form>
                                <hr>
                            </div>
                        </div>
                        <div class="col-lg-6 d-none d-lg-block bg-color">
                            <div class="p-5">
                                <div class="text-center ">
                                    <h2 class="h4 text-white-900 mb-4">Hello Friend..</h2>
                                    <p>To keep connect with us please login with your parsonal information</p>
                                    <button id="sing-in-btn" class="btn btn-custom">Sign In</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="forgot-password">
                <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-color">
                            <div class="p-5">
                                <div class="text-center ">
                                    <h2 class="h4 text-white-900 mb-4">Hello Friend..</h2>
                                    <p>To keep connect with us please login with your parsonal information</p>
                                    <button id="back-link" class="btn btn-custom">Back</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-2">Forgot Your Password?</h1>
                                <p class="mb-4">We get it, stuff happens. Just enter your email address below and we'll send you a link to reset your password!</p>
                            </div>
                                <form class="user" id="forgot-form">
                                    <div id="forgotPassErr"></div>
                                    <div class="form-group">
                                    <input type="email" name="forgot-email" class="form-control form-control-user" id="forget_email" placeholder="Enter Email Address..." required>
                                    </div>
                                    <button class="btn btn-primary btn-user btn-block" id="forgot">Reset Password</button>
                                </form>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>

      </div>

    </div>

  </div>


<!-- js directory -->
<?php require_once(ROOT_DIR."/_inc/js.php");?>

<script> 
$(document).ready(function(){
    $('#sing-up').hide();
    $('#forgot-password').hide();

    $('#sing-up-btn').click(function(){
        $('#login-in').hide();
        $('#sing-up').show();
    });
    $('#sing-in-btn').click(function(){
        $('#sing-up').hide();
        $('#login-in').show();
    });
    $('#forget-btn').click(function(){
        $('#login-in').hide();
        $('#forgot-password').show();
    });
    $('#back-link').click(function(){
        $('#login-in').show();
        $('#forgot-password').hide();
    });
    
    //register form
    $('#register-btn').click(function(e){
        if($('#register-form')[0].checkValidity()){
            e.preventDefault();
            $("#register-btn").text('Please wait.....');
            var pass = $("#reg_pass").val();
            var confpass = $("#reg_confirm_pass").val();
            if ( pass != confpass){
                $("#error-msg").text("* Password does not match..");
                $("#register-btn").text('Register Account');
            }else {
                $.ajax({
                    url     : "model/action.php",
                    type    : "post",
                    data    : $('#register-form').serialize()+"&action=register",
                    success : function(response){
                        if(response === 'reg_success'){
                            window.location = "home.php";
                        }else{
                            $('#regErrAlert').html(response);
                        }
                    }
                });
            }
        }
    });
    // login request
    $('#login').click(function(e){
        if($('#login-form')[0].checkValidity()){
            e.preventDefault();
            $('#login').text('Please wait.....');
            $.ajax({
                url     : "model/action.php",
                type    : 'post',
                data    : $('#login-form').serialize()+"&action=login",
                success : function(response){
                    $('#login').text('Login');
                    if(response == 'login'){
                        window.location = 'home.php';
                    }else{
                        $('#logErrAlert').html(response);
                    }
                }
            });
        }
    });
    // Forgot request
    $('#forgot').click(function(e){
        if ($('#forgot-form')[0].checkValidity()) {
            e.preventDefault();
            $('#forgot').text('Please wait.....');
            $.ajax({
                url     : "model/action.php",
                type    : 'post',
                data    : $('#forgot-form').serialize()+"&action=forgot",
                success : function(response){
                    $('#forgot').text('Reset Password');
                    $('#forgot-form')[0].reset();
                    $('#forgotPassErr').html(response);
                }
            });
        }

    });
});
</script>

</body>

</html>
