<?php

namespace MyApp\Utility;

class Helper{

    public static function escapeString($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    public static function messages($type, $msg){
        return "<div class='alert alert-".$type." alert-dismissible fade show' role='alert'>".$msg."
			<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
			<span aria-hidden='true'>&times;</span>
			</button>
      	</div>
        ";
    }
    public static function timeStamp( $timestamp )
    {
		date_default_timezone_set('Asia/Dhaka');
		$timestamp = strtotime($timestamp) ? strtotime($timestamp) : $timestamp ;
		$time = time() - $timestamp;
      
      	switch ($time) {
			// second
			case $time <= 60 :
			return 'Just now';
			// minute
			case $time >= 60 && $time < 3600 :
			return (round( $time / 60 ) == 1) ? 'a minute ago' : round( $time / 60 ).' minutes ago';
			// hour
			case $time >= 3600 && $time < 86400 :
			return (round( $time / 3600 ) == 1) ? 'an hour ago' : round( $time / 3600 ).' hours ago';
			// days
			case $time >= 86400 && $time < 604800 :
			return (round( $time / 86400 ) == 1) ? 'a day ago' : round( $time / 86400 ).' days ago';
			// week
			case $time >= 604800 && $time < 2600640 :
			return (round( $time / 604800 ) == 1) ? 'a week ago' : round( $time / 604800 ).' weeks ago';
			// month
			case $time >= 2600640 && $time < 31207680 :
			return (round( $time / 2600640 ) == 1) ? 'a month ago' : round( $time / 2600640 ).' months ago';
			// month
			case $time >= 31207680 :
			return (round( $time / 31207680 ) == 1) ? 'a year ago' : round( $time / 2600640 ).' years ago';
      	}
	}
	public  static function dateTime($date)
	{
		return date( 'd M, Y', strtotime( $date ) );
	}
    public static function dd($data)
    {
		echo "<pre>";
		var_dump($data);
		echo "</pre>";
    }
}



?>