<?php

namespace MyApp\Note;

use MyApp\Db\Db;
use PDO;

class Note{

    private $conn ;
    public function __construct()
    {
        $this->conn = Db::connection();
    }

    public function add( $cUserId, $noteTitle, $notecon )
    {
        $sql    = "INSERT INTO note(user_id, note_title, note) VALUES(:user_id, :notTitle, :note); ";
        $stmt   = $this->conn->prepare( $sql );
        $stmt->execute( ['user_id' => $cUserId, 'notTitle' => $noteTitle, 'note' => $notecon] );
        return true;
    }
    public function getNote($id)
    {
        $sql    = "SELECT * FROM note WHERE user_id = :id ; ";
        $stmt   = $this->conn->prepare( $sql );
        $stmt->execute( ['id' => $id] );
        $results = $stmt->fetchAll( PDO::FETCH_OBJ );
        return $results;
    }
    public function selectNote( $id )
    {
        $sql    = "SELECT note_id, note_title, note FROM note WHERE note_id = :id ; ";
        $stmt   = $this->conn->prepare( $sql );
        $stmt->execute( ['id' => $id] );
        $results = $stmt->fetch( PDO::FETCH_OBJ );
        return $results;
    }
    public function update( $update_id, $updateTitle, $updateNote )
    {
        $sql = " UPDATE note SET note_title = :note_title, note = :note, update_at = NOW() WHERE note_id = :note_id ; ";
        $stmt = $this->conn->prepare( $sql );
        $stmt->execute( ['note_title' => $updateTitle, 'note' => $updateNote, 'note_id' => $update_id ] );
        return true;
    }
    public function delete( $id )
    {
        $sql    = " DELETE FROM note WHERE note_id = :id ; ";
        $stmt   = $this->conn->prepare( $sql );
        $stmt->execute( [ 'id' => $id] );
        return true;
    }
    public function viewThisNote($note_id, $user_id)
    {
        $sql = " SELECT concat( u.first_name,' ',u.last_name ) AS full_name, u.email, n.note_title, n.note, n.created_at, n.update_at FROM note AS n JOIN users AS u ON n.user_id = u.user_id WHERE note_id = :note_id AND n.user_id = :user_id ; ";
        $stmt   = $this->conn->prepare( $sql );
        $stmt->execute( ['note_id' => $note_id, 'user_id' => $user_id] );
        $results = $stmt->fetch( PDO::FETCH_OBJ );
        return $results;
    }
    public function sendFeedback($cUserId, $sub, $feedback)
    {
        $sql = " INSERT INTO feedback(user_id, subject, feedback) VALUES(:user_id, :subject, :feedback); ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute( ['user_id' => $cUserId, 'subject' => $sub, 'feedback' => $feedback] );
        return true;
    }
    public function setNotification($cUserId, $type, $messages)
    {
        $sql = " INSERT INTO notification(user_id, type, messages) VALUES(:user_id, :type, :msg) ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute( ['user_id' => $cUserId, 'type' => $type, 'msg' => $messages] );
        return true;
    }
    public function getNotification($cUserId)
    {
        $sql = " SELECT noti_id, u.user_id, type, messages, is_seen, created_at, u.photo  FROM notification AS n JOIN users AS u ON n.user_id = u.user_id WHERE n.user_id = :id ORDER BY noti_id DESC;";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['id' => $cUserId]);
        
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    public function notiCount($cUserId)
    {
        $sql = " SELECT noti_id, u.user_id, type, messages, is_seen, created_at, u.photo FROM notification AS n JOIN users AS u ON n.user_id = u.user_id WHERE n.user_id = :id AND is_seen = 0 ;";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['id' => $cUserId]);
        
        $result = $stmt->rowCount();
        return $result;
    }
    public function allNotify($cUserId)
    {
        $sql = " SELECT * FROM notification WHERE user_id = :id ORDER BY noti_id DESC;";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['id' => $cUserId]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    public function thisNotify($id)
    {
        $sql = "SELECT is_seen FROM notification WHERE noti_id = :id";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['id' => $id]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($result['is_seen'] == 0) {
            $sql = " UPDATE notification SET is_seen = 1, seen_time = now() WHERE noti_id = :id; ";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute(['id' => $id]);
        }
        
        $sql = " SELECT * FROM notification WHERE  noti_id = :id; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['id' => $id]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
   

}




?>