<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title><?= ucfirst(basename($_SERVER['PHP_SELF'], ".php")); ?> || User management system</title>

<!-- Custom fonts for this template-->
<link href="<?= FONTAWESOM ;?>all.min.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<!-- dataTable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>
<!-- Custom styles for this template-->
<link href="<?= CSS ;?>sb-admin-2.css" rel="stylesheet">
<style>
.navbar-nav li.noti-card .nav-card{
    width: 350px;
    background-color: whitesmoke;
    box-shadow: 0px 2px 7px 0px;
    margin: 20px 20px;
    border-radius: 6px 6px 0 0;
    position: relative;
    border: 1px solid #4e73df;
    opacity: 0;
    visibility: hidden;
    transition: all 0.3s;
    position: absolute;
    top: 40px;
    right: -150px;
    z-index: 100;
    
}
.nav-card {
    width: 350px;
    background-color: whitesmoke;
    box-shadow: 0px 2px 7px 0px;
    margin: 20px 20px;
    border-radius: 6px 6px 0 0;
    position: relative;
    border: 1px solid #4e73df;
    *display: none;
    transition: all 0.3s;
    
}
.nav-card .nav-card-head {
    background: #4e73df;
    padding: 15px 15px;
    color: #fff;
    border-radius: 6px 6px 0 0;
}
.nav-card .nav-card-head h5 {
    margin-bottom: 0;
    text-transform: uppercase;
    font-size: 15px
}
.nav-card-body {
    border-bottom: 1px solid #c0c0c5;
    height: 246px;
    overflow-y: scroll;
}
.nav-card-body a:hover {
    text-decoration: none;
    background-color: #ffd;
}
.nav-card-body .user-notify {
    padding: 15px 15px;
    display: flex;
}
.nav-card-body .user-notify .user-img {
    float: left;
}
.nav-card-body .br {
    margin : 0;
    background-color: #fff;
}
.user-img img{
    width: 50px;
}
.user-msg-body {
    margin-left: 10px;
}
.user-msg-body .user-msg-time{
    color: #b7b9cc!important;
    font-size: 12px;
}
.user-notify {
    position: relative;
    background-color: #ffdecc;
}
.user-notify .circle {
    width: 10px;
    height: 10px;
    border-radius: 50%;
    background-color: #4e73df;
    position: absolute;
    top: 25px;
    right: 23px;
}
.user-msg-body .user-msg {
    color: #3a3b45;
    font-size: 14px;
}
.nav-card-foor {
    padding: 10px 15px;
    border-radius: 0 0 6px 6px;
}
.nav-card-foor a{
    display: block;
    cursor: pointer;
    color: #b7b9cc!important;
    text-align: center;
}
.nav-card .box {
    width: 20px;
    height: 20px;
    background: #4e73df;
    position: absolute;
    transform: rotate(45deg);
    top: -11px;
    left: 155px;
}

/*---- class for seen notification -----*/		
.seen {
    background-color: #fff;
}
.circle-seen {
    display: none;
}
/*---- class for seen notification/ -----*/	
.navbar-nav li.noti-card {
    position: relative;
}
.navbar-nav li.noti-card:hover .nav-card {
    opacity: 1;
    visibility: visible
}
</style>