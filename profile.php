<?php
    include_once ('header.php');
?>


<div class="container">
     <!-- main contain -->
    <div id="main-contain">
    <!-- <h3 class="text-center text-primary" style="margin-top:150px;">Loading....</h3> -->
        <!-- include page by ajax require_once -->
        <div class="card align-self-center">
            <div class="card-header border-bottom-0">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Profile</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Edit Profile</a>
                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Change Password</a>
                    </div>
                </nav>
            </div>
            <div class="card-bory">
                <div class="tab-content" id="nav-tabContent">
                    <!-- profile display tab -->
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="px-3" id="verifyMsg"></div>
                        <div class="card-deck m-1">
                            <div class="card border-primary">
                                <div class="card-body">
                                    <p class="card-text p-2 rounded" style="border: 1px solid #d2caca;" >Full Name : <?= $currentUser['first_name'].' '.$currentUser['last_name'] ; ?></p>

                                    <p class="card-text p-2 rounded" style="border: 1px solid #d2caca;" >Email : <?= $currentUser['email'] ; ?></p>

                                    <p class="card-text p-2 rounded" style="border: 1px solid #d2caca;" >Phone NO : <?= $currentUser['phone'] ; ?></p>

                                    <p class="card-text p-2 rounded" style="border: 1px solid #d2caca;" >Gender : <?= ucfirst( $currentUser['gender'] ) ; ?></p>

                                    <p class="card-text p-2 rounded" style="border: 1px solid #d2caca;" >Date Of Birth : <?= date('d M Y', strtotime( $currentUser['dateOfBirth'])) ; ?></p>

                                    <p class="card-text p-2 rounded" style="border: 1px solid #d2caca;" >Registered ON : <?= date('d M Y', strtotime( $currentUser['create_at'] )) ; ?></p>
                                    
                                    <p class="card-text p-2 rounded" style="border: 1px solid #d2caca;" >Verify Account : <?= $currentUser['is_verified'] == 0 ? '<span class="text-danger"> NO</span> <a class="float-right" id="verify_email" href="#">Verify Now ?</a>' : '<span class="text-success"> Yes</span>' ; ?> </p>
                                </div>
                            </div>
                            <div class="card border-primary">
                                <div class="card-body">
                                    <img class="img-fluid img-thumbnail" src="<?= USER_IMG.$users->cUserImage( $currentUser['gender'], $currentUser['photo'] )?>" width="450" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- update profile tab -->
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div class="card-deck m-1">
                            <div class="card border-primary">
                                <div class="card-body">
                                    <img class="img-fluid img-thumbnail" src="<?= USER_IMG.$users->cUserImage( $currentUser['gender'], $currentUser['photo'] )?>" width="450" alt="">
                                </div>
                            </div>
                            <div class="card border-primary">
                                <div class="card-body">
                                    <form action="" enctype="multipart/form-data" id="update-form">
                                        <input type="hidden" name="oldImg" value="<?= $currentUser['photo'] ; ?>">
                                        <input type="hidden" name="cUser_id" value="<?= $cUserId ; ?>">
                                        <div class="form-group">
                                            <label class="sr-only" for="firstName">First name</label>
                                            <div class="input-group mb-2 mr-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">First name</div>
                                                </div>
                                                <input type="text" name="firstName" value="<?= $currentUser['first_name'] ;?>" class="form-control" id="firstName">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="sr-only" for="lastName">Last name</label>
                                            <div class="input-group mb-2 mr-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">Last name</div>
                                                </div>
                                                <input type="text" name="lastName" value="<?= $currentUser['last_name'] ;?>" class="form-control" id="lastName">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="sr-only" for="phone">Phone</label>
                                            <div class="input-group mb-2 mr-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">Phone</div>
                                                </div>
                                                <input type="number" name="phone" value="<?= $currentUser['phone'] ;?>" class="form-control" id="phone">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="sr-only" for="gender">Gender</label>
                                            <div class="input-group mb-2 mr-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">Gender</div>
                                                </div>
                                                <select class="form-control" name="gender" id="gender">
                                                    <option <?php if ( !$currentUser['gender'] ) echo 'selected'; ?> value="">Select One</option>
                                                    <option <?php if ( $currentUser['gender'] == 'male' ) echo 'selected'; ?> value="male">Male</option>
                                                    <option <?php if ( $currentUser['gender'] == 'female' ) echo 'selected'; ?> value="female">Female</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="sr-only" for="dateOfBirth">Date Of Birth</label>
                                            <div class="input-group mb-2 mr-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">Date Of Birth</div>
                                                </div>
                                                <input value="<?= $currentUser['dateOfBirth']?>" type="date" value="" name="dateOfBirth" class="form-control" id="dateOfBirth">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="sr-only" for="user_img">User Image</label>
                                            <div class="input-group mb-2 mr-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">User Image</div>
                                                </div>
                                                <input type="file" value="" name="user_img" class="form-control" id="user_img">
                                            </div>
                                        </div>
                                        <button type="submit" id="update_btn" class="btn btn-primary btn-block">Update Information</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- change user password tab -->
                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                        <div class="card-deck m-1">
                            <div class="card border-primary">
                                <div class="card-header bg-primary">
                                    <h3 class="text-white m-0">Change Password</h3>
                                </div>
                                <div class="card-body">
                                    <form action="" id="changePassForm">
                                        <div id="serverErr"></div> 
                                        <input type="hidden" value="<?= $currentUser['user_id'] ; ?>" name="user_id"> 
                                        <div class="form-group">
                                            <label for="currentPassword">Current Password</label>
                                            <input class="form-control" type="password" name="currentPassword" id="currentPassword" placeholder="Enter Your old password" required>
                                            <small id="passErrMsg"></small>
                                        </div>
                                        <div class="form-group">
                                            <label for="newPassword">New Password</label>
                                            <input class="form-control" type="password" name="newPassword" id="newPassword" placeholder="Enter Your new password" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="confirmPassword">Confirm Password</label>
                                            <input class="form-control" type="password" name="confirmPassword" id="confirmPassword" placeholder="Enter confirm password" required>
                                            <small id="errMsg"></small>
                                        </div>
                                        
                                        <button class="btn btn-primary btn-block" type="submit" id="change_pass_btn">Change Password</button>
                                    </form>
                                </div>
                            </div>
                            <div class="card border-primary">
                                <div class="card-body">
                                    <img class="img-fluid img-thumbnail h-100" src="<?= USER_IMG.'pass.gif' ; ?>" width="450" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- change user password tab end -->
                </div>
            </div>
        </div>
    </div>
</div>


<!-- js directory -->
<?php require_once(ROOT_DIR."/_inc/js.php");?>
<?php require_once('ajax.php'); ?>

<script>
$(document).ready(function(){

    $('#update-form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url     : 'model/action.php',
            type    : 'POST',
            processData : false,
            contentType : false,
            cache   : false,
            data    : new FormData(this),
            success : function (response) {
                if (response == 'success') {
                    Swal.fire({
                        icon    : 'success',
                        title   : 'Update user information successfully!'
                    }).then((result) => {
                        location.reload();
                    });
                } else {
                    Swal.fire({
                        icon    : 'error',
                        title   : 'Oops...',
                        text    : 'Update user information not successfully!',
                    });
                }
                
            }
        });
    });
    // checked current password
    $('#currentPassword').keyup(function(){
        var cPassword = $(this).val();
        var user_id = "<?= $currentUser['user_id']; ?>";
        $.post('model/action.php',{
            user_id : user_id,
            cPassword : cPassword
        },function(data, status){
            $('#passErrMsg').html(data);
        });
    });
    // match password
    $('#confirmPassword').keyup(function () {
        var password = $("#newPassword").val();
        var confirmPassword = $("#confirmPassword").val();

        if (password != confirmPassword)
            $("#errMsg").html('<span class="text-danger">Passwords do not match!</span>');
        else
            $("#errMsg").html('<span class="text-success">Passwords match.</span>');
    });   
    // Change password
    $('#change_pass_btn').click(function(e){
        if ( $('#changePassForm')[0].checkValidity() ) {
            e.preventDefault();
            var password = $("#newPassword").val();
            var confirmPassword = $("#confirmPassword").val();
            $('#change_pass_btn').text('Please wait.....');
            if ( password != confirmPassword ) {
                $('#errMsg').text('*Sorry! Password does match!');
            } else {
                $.ajax({
                    url     : "model/action.php",
                    type    : 'post',
                    data    : $('#changePassForm').serialize()+"&action=changePass",
                    success : function(response){
                        $('#change_pass_btn').text('Change Password');
                        $('#changePassForm')[0].reset();
                        $('#serverErr').html(response);
                        //console.log(response);
                    }
                });
            }
            
        }
    });
    // verify email
    $('#verify_email').click(function(e) {
        e.preventDefault();
        $('#verify_email').text('Please wait...');

        $.ajax({
            url     : 'model/action.php',
            type    : 'POST',
            data    : { action : 'verified_email'},
            success : function (response) {
                $('#verifyMsg').html(response);
                $('#verify_email').text('Verify Now ?');
            }
        });
    });
    
});
</script>
</body>
</html> 