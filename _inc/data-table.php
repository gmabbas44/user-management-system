<?php

    use MyApp\Note\Note;
    include_once realpath($_SERVER['DOCUMENT_ROOT'].'/user-management-system/bootstrap.php');
    if (!isset($_POST['action'])) header ('location: '.WEB_ROOT.'home.php');
    include_once (MODEL.'session.php');

    $note = new Note();
    $userNotes = $note->getNote($cUserId);

?>
<div class="card border-primary">
<div class="card-header bg-primary">
    <h2 class="float-left text-white">All Note</h2>
    <button type="button" class="btn btn-light float-right" data-toggle="modal" data-target="#addNote"><i class="far fa-plus-square"></i> Add New</button>
</div>

<div class="card-body">
    <table class="table table-bordered">
        <thead>
            <th>SL</th>
            <th>Title</th>
            <th>Note</th>
            <th class="text-center">Action</th>
        </thead>
        <tbody>
            <?php foreach ($userNotes as $userNote):?>
            <tr>
                <td class="align-middle"><?= $userNote->note_id ;?></td>
                <td width="34%"><?= $userNote->note_title ;?></td>
                <td><?= substr($userNote->note,0,40) ;?>....</td>
                <td width='10%' class="text-center align-middle">
                    <a href="#" id="<?= $userNote->note_id ;?>" class="text-info info-btn" data-toggle="tooltip" data-placement="left" title="View User Information"><i class="fas fa-info-circle"></i></a>
                    <a href="#" id="<?= $userNote->note_id ;?>" class="btn-update" data-toggle="modal" data-target="#update-box" ><i title="Edit User Information" data-toggle="tooltip" data-placement="top" class="fas fa-user-edit"></i></a>
                    <a href="#" id="<?= $userNote->note_id ;?>" class="text-danger delete-btn" data-toggle="tooltip" data-placement="right" title="Delete User Information"><i class="far fa-trash-alt"></i></i></a>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>  
</div>
