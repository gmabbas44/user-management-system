-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 21, 2020 at 08:42 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `user_info`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `username`, `email`, `password`, `created_at`, `modified_at`) VALUES
(1, 'Admin', 'admin@email.com', '12345', '2020-04-17 05:50:31', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `fd_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `feedback` mediumtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `replied` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`fd_id`, `user_id`, `subject`, `feedback`, `created_at`, `replied`) VALUES
(1, 2, 'Hello', 'Hello! this is Gm Abbas Uddin', '2020-04-11 16:26:24', 0),
(2, 2, 'This is test feedback', 'Hello! this is Gm Abbas Uddin', '2020-04-11 16:36:43', 0),
(3, 2, 'hello', 'this is feedback', '2020-04-14 14:35:21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `note`
--

CREATE TABLE `note` (
  `note_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `note_title` text NOT NULL,
  `note` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `note`
--

INSERT INTO `note` (`note_id`, `user_id`, `note_title`, `note`, `created_at`, `update_at`) VALUES
(1, 2, 'Full Stack Web Application Developer', 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Et, sapiente! Tenetur recusandae eos repellat perspiciatis iusto explicabo perferendis deleniti voluptate, sit deserunt nisi illo corporis velit, quidem dolorem. Quisquam, perspiciatis!', '2020-04-08 09:35:32', '2020-04-08 09:35:32'),
(3, 2, 'Web app develop hello', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat porro odio iusto, ab, esse cum tempore atque dolorem nesciunt ipsum iste! Rerum porro sed quis quo quaerat voluptate sunt laudantium?', '2020-04-08 09:55:52', '2020-04-08 17:03:02'),
(5, 2, 'Graphic design 123', 'Graphic design is the process of visual communication and problem-solving through the use of typography, photography, iconography and illustration.', '2020-04-08 10:19:11', '2020-04-08 15:56:45'),
(6, 1, 'php engineer', 'this is Gm Abbas Udidn, a professional web App developer.', '2020-04-08 10:23:02', '2020-04-08 10:23:02'),
(7, 2, 'Java programmer', 'hello, dear programmer, your future are bright, just wait and continue your practice,', '2020-04-08 16:00:28', '2020-04-11 10:19:22'),
(8, 2, 'php zend certified engineer', 'The Zend company offers the Zend Certified PHP Engineer certification programme which covers different areas of developing applications in PHP 5.5. The topics that you have to learn for the exam are grouped in several categories like PHP Basics, Object Oriented Programming, Security or Arrays', '2020-04-12 19:13:29', '2020-04-12 19:13:29'),
(9, 2, 'c programmer', 'C is a powerful general-purpose programming language. It can be used to develop software like operating systems, databases, compilers, and so on.', '2020-04-13 16:55:19', '2020-04-13 16:55:19'),
(10, 2, 'Python Programming Bootcamp', 'Learn Python Programming From The Basics All The Way to Creating your own Apps and Games! Join Over 40 Million Students Already Learning Online With Udemy. Selenium.', '2020-04-14 14:09:56', '2020-04-17 12:00:06'),
(11, 2, 'C Sharp (programming language)', 'C# (pronounced see sharp, like the musical note Câ™¯, but written with the number sign) is a general-purpose, multi-paradigm programming language encompassing strong typing, lexically scoped, imperative, declarative, functional, generic, object-oriented (class-based), and component-oriented programming disciplines.', '2020-04-21 16:19:24', '2020-04-21 16:19:24');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `noti_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `messages` varchar(255) NOT NULL,
  `is_seen` tinyint(4) NOT NULL DEFAULT '0',
  `seen_time` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`noti_id`, `user_id`, `type`, `messages`, `is_seen`, `seen_time`, `created_at`) VALUES
(1, 2, 'user', 'You add a new note', 1, '2020-04-16 18:43:33', '2020-04-13 16:55:19'),
(2, 2, 'user', 'You add a new note', 1, '2020-04-16 18:44:25', '2020-04-14 14:09:56'),
(3, 2, 'user', 'You update a new note', 1, '2020-04-16 18:51:50', '2020-04-14 14:10:36'),
(4, 2, 'user', 'You add feedback from Admin', 1, '2020-04-16 18:58:54', '2020-04-14 14:35:21'),
(5, 2, 'user', 'You update a new note', 1, '2020-04-16 18:47:15', '2020-04-15 16:14:13'),
(6, 2, 'user', 'You update a new note', 1, '2020-04-17 18:00:43', '2020-04-17 12:00:06'),
(7, 2, 'user', 'You add a new note', 1, '2020-04-21 22:40:15', '2020-04-21 16:19:25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `dateOfBirth` date NOT NULL,
  `photo` varchar(100) NOT NULL,
  `token` varchar(100) NOT NULL,
  `token_expire` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_verified` tinyint(2) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `password`, `phone`, `gender`, `dateOfBirth`, `photo`, `token`, `token_expire`, `create_at`, `modified_at`, `is_verified`, `is_deleted`) VALUES
(1, 'abbas', 'uddin ', 'nikil754@gmail.com', '852', '', 'male', '0000-00-00', '', '', '2020-04-19 16:51:12', '2020-04-05 07:09:15', '2020-04-05 11:55:07', 0, 0),
(2, 'Khaleda Akter', 'Nodi', 'khaladaakter382616@gmail.com', '12345', '01814953905', 'female', '1997-12-10', 'userId_2_5e9099cf94f89.png', '', '2020-04-21 14:23:03', '2020-04-06 12:50:56', '2020-04-06 12:50:56', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_online`
--

CREATE TABLE `user_online` (
  `s_id` int(11) NOT NULL,
  `session` varchar(200) NOT NULL,
  `session_time` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_online`
--

INSERT INTO `user_online` (`s_id`, `session`, `session_time`) VALUES
(1, 'daictkm7iuc5ardvqbvmgn56t9', '1587489847'),
(2, '2p69u910bqjgmgu4llu9q7kuar', '1587494577');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`fd_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `note`
--
ALTER TABLE `note`
  ADD PRIMARY KEY (`note_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`noti_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_online`
--
ALTER TABLE `user_online`
  ADD PRIMARY KEY (`s_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `fd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `note`
--
ALTER TABLE `note`
  MODIFY `note_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `noti_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_online`
--
ALTER TABLE `user_online`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT `note_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
