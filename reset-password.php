<?php

use MyApp\Users\Users;
use MyApp\Utility\Helper;

include_once realpath($_SERVER['DOCUMENT_ROOT'].'/user-management-system/bootstrap.php');

    if (isset($_GET['email']) && isset($_GET['token']) && !empty($_GET['email']) && !empty($_GET['token'])) {
        $email = $_GET['email'];
        $token = $_GET['token'];
        $users = new Users;
        $is_user = $users->resetPassRequest($email, $token);

        if ($is_user != null) {
            if (isset($_POST['reset-btn'])) {
                $password = $_POST['pass'];
                $confirmPass = $_POST['confirmPass'];

                if ($password !== $confirmPass) {
                   $msg = Helper::messages('danger',"<strong>Oops!</strong> Password did not matched!");
                } else {
                    if ($users->updatePass($email, $password)) {
                        $msg = Helper::messages('success','<strong>Wow!</strong> Password Change successfully!<br> Please <a href="index.php">Login</a>');
                    } else {
                        $msg = Helper::messages('warning',"<strong>Oops!</strong> Something wrong! Please try again.");
                    }
                }
            }
        } else {
            $msg = Helper::messages('warning',"<strong>Oops!</strong> User not found...<br> Please <a href='index.php'>Login</a>");
        }
    }else {
        header('location: '.WEB_ROOT);
        die;
    }

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- css directory -->
    <?php require_once(ROOT_DIR."/_inc/css.php");?>
</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div id="reset-box">
                <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-color">
                            <div class="p-5">
                                <div class="text-center ">
                                    <h2 class="h4 text-white-900 mb-4">Hello Firend</h2>
                                    <div class="text-center forgot-img">
                                        <img class="w-100 " src="img/user-1.gif" alt="img">
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Reset password</h1>
                                    <?php
                                        if(isset($msg)) echo $msg ;
                                    ?>
                                </div>
                                <?php if ($is_user != null):?>
                                <form class="user" method="post">
                                    <div id="logErrAlert"></div>
                                    <div class="form-group">
                                    <input type="password" name="pass" class="form-control form-control-user" placeholder="New Password" required>
                                    </div>
                                    <div class="form-group">
                                    <input type="password" name="confirmPass" class="form-control form-control-user" placeholder="Confirm Password"  required>
                                    </div>
                                    <button id="reset-btn" name="reset-btn" type="submit" class="btn btn-primary btn-user btn-block">Reset Password</button>
                                </form>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
           
        </div>

      </div>

    </div>

  </div>

<!-- js directory -->
<?php require_once(ROOT_DIR."/_inc/js.php");?>


</body>

</html>
