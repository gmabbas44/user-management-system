<?php
    include_once ('header.php');
?>


<div class="container">
    <!-- main contain -->
    <div class="row justify-content-center">
        <div class="col-md-8">
            <?php if ( $currentUser['is_verified'] == 0 ) : ?>
            <div class="card">
                <div class="card-header bg-primary text-center text-white">
                    FeedBack
                </div>
                <div class="card-body">
                    <form action="" method="post" id="feedback-form">
                        <div class="form-group">
                            <input type="text" name="subject" class="form-control" id="subject" placeholder="Enter your subjcet" required>
                        </div>
                        <div class="form-group">
                            <textarea name="feedback" class="form-control" id="feedback" cols="30" rows="8" placeholder="Enter your feedback..." required></textarea>
                        </div>
                        <button id="feedback-btn" class="btn btn-primary btn-block"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send your feedback</button>
                    </form>
                </div>
            </div>
            <?php else : ?>
                <h2 class="text-warning text-center">:)Please! Verify you email..</h2>
            <?php endif; ?>
        </div>
    </div>
</div>


<!-- js directory -->
<?php require_once(ROOT_DIR."/_inc/js.php");?>
<?php require_once('ajax.php'); ?>

<script>
$(document).ready(function(){

    // send feedback
    $('#feedback-btn').click(function(e){
        if ($('#feedback-form')[0].checkValidity()){
            e.preventDefault();
            $('#feedback-btn').text('Please wail...');
            $.ajax({
                url : 'model/process.php',
                type : 'POST',
                data : $('#feedback-form').serialize()+"&action=feedback",
                success : function (response) {
                    $('#feedback-btn').text('Send your feedback');
                    $('#feedback-form')[0].reset();
                    if (response == 'success' ) {
                        Swal.fire({
                            icon    : 'success',
                            titie   : 'success',
                            text    : 'Feedback send successfully!'
                        });
                    } else {
                        Swal.fire({
                            icon    : 'warning',
                            titie   : 'warning',
                            text    : 'Feedback send not successfully!'
                        });
                    }
                }
            });
        }
    });

});
</script>
</body>
</html> 