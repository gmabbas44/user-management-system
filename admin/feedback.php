<?php

include_once realpath($_SERVER['DOCUMENT_ROOT'].'/user-management-system/bootstrap.php');

include (ADMIN.'model/session.php');

?>

<!DOCTYPE html>
<html lang="en">

<head>
<!-- css directory -->
<?php require_once(ROOT_DIR."/_inc/css.php");?>
<!-- Custom styles for this template-->
<link href="<?= CSS ;?>sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

<?php require_once( ADMIN.'admin_inc/header.php' ); ?>



<div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="card">
			<div class="card-header bg-primary text-white ">
				<h4 class="text-center">Feedback</h4>
			</div>
			<div class="card-body">
				<div id="showFeedback" class="table-responsive">
					
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="replyFeedback" tabindex="-1" role="dialog" aria-labelledby="replyFeedback" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="replyFeedback">Admin Feedback</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#" method="post" id="reply-from">
                    <div class="form-group">
                    <label class="sr-only" for="message">message</label>
                    <textarea class="form-control" id="message" rows="5" placeholder="Write your reply" required></textarea>
                    </div>
                    <button id="reply-btn" class="btn btn-primary btn-block"><i class="fas fa-reply"></i> Reply</button>
                </form>
            </div>
        </div>
    </div>
</div>


<?php require_once( ADMIN.'admin_inc/footer.php' ); ?>
  <!-- Bootstrap core JavaScript-->
<?php require_once(ROOT_DIR."/_inc/js.php");?>

<script>
$(document).ready( function () {

    showFeedback();
	function showFeedback() {
		$.ajax({
			url		: 'model/action.php',
			type	: 'POST',
			data	: { action : 'showFeedback' },
			success : function (reaponse) {
				$('#showFeedback').html(reaponse);
				$('table').DataTable({
					order : [0, 'desc']
				});
			}
		});
	}
    var f_id, user_id;
    $('body').on('click', '.reply-btn', function(e) {
        e.preventDefault();
        f_id = $(this).attr('f_id');
        user_id = $(this).attr('user_id');
    });

    $('#reply-btn').click(function(e) {
        if ( $('#reply-from')[0].checkValidity() ) {
            let mgs = $('#message').val();
            e.preventDefault();
            $('#reply-btn').text('Please wait.....');
            
            $.ajax({
                url     : 'model/action.php',
                type    : 'POST',
                data    : {
                    f_id    : f_id,
                    u_id    : user_id,
                    mgs     : mgs
                },
                success : function( response ) {
                    $('#reply-btn').html('<i class="fas fa-reply"></i> Reply');
                    $('#replyFeedback').modal('hide');
                    $('#reply-from')[0].reset();
                    Swal.fire({
                        title   : 'Added!',
                        text    : 'Reply send successfully!',
                        icon    : 'success'
                    });
                    showFeedback();
                }

            });
        }
    });
});
    
</script>


</body>

</html>
