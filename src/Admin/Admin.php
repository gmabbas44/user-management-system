<?php

namespace MyApp\Admin;

use MyApp\Db\Db;
use \PDO;
class Admin {

    public $conn; 

    public function __construct()
    {
        $this->conn = Db::connection();
    }
    
    public function adminLogin($email)
    {
        $sql = " SELECT email, password FROM admin WHERE email = :email; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['email' => $email]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function existAdmin($email)
    {
        $sql = " SELECT email FROM admin WHERE email = :email ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['email' => $email]);
        $res = $stmt->fetch(PDO::FETCH_ASSOC);
        return  $res['email'] == $email ? true : false ;
    }
    public function currentAdmin($email)
    {
        $sql = " SELECT * FROM admin WHERE email = :email ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['email' => $email]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    public function totalCount($tableName)
    {
        $sql = " SELECT * FROM $tableName ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->rowCount();
    }
    public function is_verified($status)
    {
        $sql = " SELECT is_verified FROM users WHERE is_verified = :status ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['status' => $status]);
        return $stmt->rowCount();
    }
    public function genderPer()
    {
        $sql = " SELECT gender, COUNT(*) AS number FROM users WHERE gender != '' GROUP BY gender ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function verifyPer()
    {
        $sql = " SELECT is_verified, COUNT(*) AS number FROM users GROUP BY is_verified ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function activeUserOnline()
    {
        $time = time();
        $time_out_in_seconds = 5;
        $time_out = $time - $time_out_in_seconds;
        $sql = " SELECT session FROM user_online WHERE session_time > :time_out ;" ;
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['time_out' => $time_out]);
        return $stmt->rowCount();
    }
    public function allUser($is_deleted)
    {
        $sql = " SELECT user_id, photo, concat(first_name,' ',last_name) AS fullname, email, phone, gender, is_verified FROM users WHERE is_deleted = :is_deleted";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['is_deleted' => $is_deleted]);
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    public function getUserInfo($user_id)
    {
        $sql = " SELECT * FROM users WHERE user_id = :user_id AND is_deleted = 0 ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['user_id' => $user_id]);
        return $stmt->fetch(PDO::FETCH_OBJ);
    }
    public function delAndRestore($id, $vl)
    {
        $sql = " UPDATE users SET is_deleted = :vl WHERE user_id = :id ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['id' => $id, 'vl' => $vl]);
        return true;
    }
    public function trash($del_id)
    {
        $sql = " DELETE FROM users WHERE user_id = :del_id ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['del_id' => $del_id]);
        return true;
    }
    public function allNote()
    {
        $sql = " SELECT n.note_id, concat(first_name,' ',last_name) AS fullname, email, n.note_title, n.note, n.created_at, n.update_at FROM note AS n JOIN users AS u ON n.user_id = u.user_id ORDER BY note_id DESC ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    public function delNote($note_del_id)
    {
        $sql = " DELETE FROM note WHERE note_id = :id ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['id' => $note_del_id]);
        return true;
    }

    public function allfeedback()
    {
        $sql = " SELECT f.fd_id, f.user_id, f.subject, f.feedback, f.created_at, u.first_name FROM feedback AS f JOIN users AS u ON f.user_id = u.user_id WHERE replied != 1 ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    
    public function replyFeedback($u_id, $mgs)
    {
        $sql = " INSERT INTO `notification`(`user_id`, `type`, `messages`) VALUES (:u_id, 'Admin', :mgs) ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['u_id' => $u_id,'mgs' => $mgs]);
        return true;
    }

    public function feedbackReplied($f_id)
    {
        $sql = " UPDATE feedback SET replied = 1 WHERE fd_id = :fd_id ; ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['fd_id' => $f_id]);
        return true;
    }
}





?>