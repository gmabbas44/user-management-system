<?php

use MyApp\Admin\Admin;
use MyApp\Users\Users;
use MyApp\Utility\Helper;

include_once realpath($_SERVER['DOCUMENT_ROOT'].'/user-management-system/bootstrap.php');
$admin = new Admin;
$users = new Users;

if ( isset( $_POST['action'] ) && $_POST['action'] == 'login' ) {
    $email      = Helper::escapeString( $_POST['email'] );
    $password   = Helper::escapeString( $_POST['password'] );
    $loginAdmin = $admin->adminLogin( $email );
    if ($loginAdmin != null) {
        if ($loginAdmin['password'] == $password) {
            echo "login";
            $_SESSION['email'] = $loginAdmin['email'];
            exit;
        } else {
            echo Helper::messages('warning', '<strong>Opps!</strong> Your password wrong');
        }
    }  else {
        echo Helper::messages('warning', '<strong>Opps!</strong> user not found');
    }
}
if ( isset( $_POST['email'] ) && !empty($_POST['email'])) {
    $email = Helper::escapeString( $_POST['email'] );
    $res = $admin->existAdmin($email);
    if ($res == true) {
        echo '<span class="text-success">Admin found</span>';
    } else {
     echo '<span class="text-danger">Admin not found</span>';
    }
 }

 if (isset( $_POST['tableName'] ) && !empty($_POST['tableName'])) {
    echo $admin->totalCount($_POST['tableName']);
 }

 if (isset( $_POST['action']) && $_POST['action'] == 'userOnline' ) {
    echo $admin->activeUserOnline();
 }

 if ( isset( $_POST['showUser'] ) && $_POST['showUser'] == 'showUser') {
    $allUsers = $admin->allUser(0);

    $output = '';

    foreach ($allUsers as $user) {
        $photo = $users->cUserImage($user->gender, $user->photo);
        $is_verified = $user->is_verified == 1 ? '<i class="far fa-check-circle text-success"></i>' : '<i class="far fa-times-circle text-danger"></i>';
        $output .= '
            <tr>
                <td class="align-middle">'.$user->user_id.'</td>
                <td><img width="40" class="img-profile rounded-circle" src="'.IMG.'user-img/'.$photo.'" alt=""></td>
                <td class="align-middle">'.$user->fullname.'</td>
                <td class="align-middle">'.$user->email.'</td>
                <td class="align-middle">'.$user->phone.'</td>
                <td class="align-middle">'.ucfirst( $user->gender ).'</td>
                <td class="align-middle text-center">'.$is_verified.'</td>
                <td class="align-middle text-center">
                    <a href="#" id="'.$user->user_id.'" class="text-info info-btn" title="View User Information" data-toggle="modal" data-target="#userInfo"><i class="fas fa-info-circle"></i></a>
                    <a href="#" id="'.$user->user_id.'" class="delete-btn text-danger"><i title="Delete User Information" class="fas fa-trash-alt"></i></a>
                </td>
            </tr>';
    }
    echo $output;
 }

if ( isset( $_POST['user_id']) && !empty( $_POST['user_id'] ) ) {
    $user_id = Helper::escapeString( $_POST['user_id'] );
    $userInfo = $admin->getUserInfo($user_id);
    // echo json_encode($userInfo);
    $is_verified = $userInfo->is_verified == 1 ? ' <i class="far fa-check-circle text-success"></i> Yes' : ' <i class="far fa-times-circle text-danger"></i> NO';
    echo '<div class="text-center mb-2">
    <img width="100" class="rounded-circle" src="'.IMG.'user-img/'.$users->cUserImage($userInfo->gender, $userInfo->photo).'" alt="user Image">
    </div>
    <p class=" p-1 rounded" style="border: 1px solid #d2caca;">Full Name : '.$userInfo->first_name.'</p>
    <p class=" p-1 rounded" style="border: 1px solid #d2caca;">Email : '.$userInfo->email.'</p>
    <p class=" p-1 rounded" style="border: 1px solid #d2caca;">Gender : '.ucfirst($userInfo->gender).'</p>
    <p class=" p-1 rounded" style="border: 1px solid #d2caca;">Phone : '.$userInfo->phone.'</p>
    <p class=" p-1 rounded" style="border: 1px solid #d2caca;">Date Of Birth : '.Helper::dateTime( $userInfo->dateOfBirth ).'</p>
    <p class=" p-1 rounded" style="border: 1px solid #d2caca;">Registered ON : '.Helper::dateTime( $userInfo->create_at ).'</p>
    <p class=" p-1 rounded" style="border: 1px solid #d2caca;">Verify Account : '.$is_verified.'</p> ';
}

if ( isset( $_POST['del_user_id'] ) && !empty( $_POST['del_user_id'] )) {
    $del_user_id = Helper::escapeString( $_POST['del_user_id'] );
    $admin->delAndRestore($del_user_id, 1);
}

if ( isset( $_POST['showDelUser']) ) {
    $delUsers = $admin->allUser(1);
    $output = '';
    foreach ($delUsers as $delUser) {
        $photo = $users->cUserImage($delUser->gender, $delUser->photo);
        $is_verified = $delUser->is_verified == 1 ? '<i class="far fa-check-circle text-success"></i>' : '<i class="far fa-times-circle text-danger"></i>';
        $output .= '
        <tr>
            <td class="align-middle">'.$delUser->user_id.'</td>
            <td><img width="40" class="img-profile rounded-circle" src="'.IMG.'user-img/'.$photo.' " alt="user image"></td>
            <td class="align-middle">'.$delUser->fullname.'</td>
            <td class="align-middle">'.$delUser->email.'</td>
            <td class="align-middle">'.$delUser->phone.'</td>
            <td class="align-middle">'.ucfirst( $delUser->gender ) .'</td>
            <td class="align-middle text-center">'.$is_verified.'</td>
            <td class="align-middle text-center">
                <a href="#" id="'. $delUser->user_id .'" class="text-info restore-btn" title="Restore Users" data-toggle="modal" data-target="#userInfo"><i class="fas fa-trash-restore"></i></a>
                <a href="#" id="'.$delUser->user_id .'" class="delete-btn text-danger"><i title="Confirm User Delete" class="fas fa-trash-alt"></i></a>
            </td>
        </tr>';
    }
    echo $output;
}

if ( isset( $_POST['restore_id'] ) && !empty( $_POST['restore_id'] )) {
    $restore_id = Helper::escapeString( $_POST['restore_id'] );
    $admin->delAndRestore($restore_id, 0);
}

if ( isset( $_POST['del_id'] ) && !empty( $_POST['del_id'] )) {
    $del_id = $_POST['del_id'];
    $admin->trash($del_id);
}

if (isset( $_POST['action']) && $_POST['action'] == 'showAllNote') {
    $notes = $admin->allNote();
    if ($notes) {
        $output = '';
        foreach ($notes as $note) {
            $output .= '
                <tr>
                    <td>'.$note->note_id.'</td>
                    <td>'.$note->fullname.'</td>
                    <td>'.$note->email.'</td>
                    <td>'.$note->note_title.'</td>
                    <td>'.substr($note->note,0,20).'</td>
                    <td>'.Helper::dateTime( $note->created_at ).'</td>
                    <td>'.Helper::dateTime( $note->update_at ).'</td>
                    <td class="text-center"> <a href="#" id="'.$note->note_id.'" class="delete-btn text-danger"><i title="Confirm Note Delete" class="fas fa-trash-alt"></i></a></td>
                </tr>
            ';
        }
        echo $output;
    } else {
        echo "<h3 class='text-center text-danger'>:) NO nore here</h3>";
    }
}

if (isset( $_POST['note_del_id'] ) && !empty( $_POST['note_del_id'])) {
   $note_del_id = $_POST['note_del_id'];
   $admin->delNote($note_del_id);
}

if (isset( $_POST['action']) && $_POST['action'] == 'showFeedback') {
    $allfeedback = $admin->allfeedback();
    if ($allfeedback) {
        $output = '<table class="table table-sm table-bordered table-striped border-light">
        <thead class="text-center">
            <th>Sl</th>
            <th>User</th>
            <th>Title</th>
            <th>Feedback</th>
            <th>Created at</th>
            <th>Reply</th>
        </thead>
        <tbody>';
        foreach ($allfeedback as $feedback) {
            $output .= '
                <tr>
                    <td>'.$feedback->fd_id.'</td>
                    <td>'.$feedback->first_name.'</td>
                    <td>'.$feedback->subject.'</td>
                    <td>'.$feedback->feedback.'</td>
                    <td>'.Helper::timeStamp( $feedback->created_at ).'</td>
                    <td class="text-center"> <a href="#" user_id="'.$feedback->user_id.'" f_id="'.$feedback->fd_id.'" data-toggle="modal" data-target="#replyFeedback" class="reply-btn text-danger"><i class="fas fa-reply"></i></a></td>
                </tr>
            ';
        }
        $output .= '</tbody>
        </table>';
        echo $output;
    } else {
        echo "<h3 class='text-center text-danger'>:) NO feedback here</h3>";
    }
}

if (isset( $_POST['mgs'])) {
    $f_id   = $_POST['f_id'];
    $u_id   = $_POST['u_id'];
    $mgs    = Helper::escapeString( $_POST['mgs'] );
    $admin->replyFeedback($u_id, $mgs);
    $admin->feedbackReplied($f_id);
}


?>