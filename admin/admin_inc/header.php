<!-- Page Wrapper -->
<div id="wrapper">

<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= ADMIN_WEB_ROOT.'deshboard.php'; ?>">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">UMS</div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">
  <!-- Nav Item - Dashboard -->
  <li class="nav-item <?= basename($_SERVER['PHP_SELF']) == 'deshboard.php' ? 'active' : '' ; ?>">
    <a class="nav-link" href="<?= ADMIN_WEB_ROOT.'deshboard.php'; ?>" id="dashboard-nav">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Dashboard</span></a>
  </li>

  <hr class="sidebar-divider my-0">

  <li class="nav-item <?= basename($_SERVER['PHP_SELF']) == 'users.php' ? 'active' : '' ; ?>">
    <a class="nav-link" href="<?= ADMIN_WEB_ROOT.'users.php'; ?>" id="users-nav">
      <i class="fas fa-fw fa-users"></i>
      <span>Users</span></a>
  </li>

  <hr class="sidebar-divider my-0">

  <li class="nav-item <?= basename($_SERVER['PHP_SELF']) == 'notes.php' ? 'active' : '' ; ?>">
    <a class="nav-link" href="<?= ADMIN_WEB_ROOT.'notes.php'; ?>" id="notes-nav">
    <i class="fas fa-sticky-note"></i>
      <span>Notes</span></a>
  </li>

  <hr class="sidebar-divider my-0">

  <li class="nav-item <?= basename($_SERVER['PHP_SELF']) == 'feedback.php' ? 'active' : '' ; ?>">
    <a class="nav-link" href="<?= ADMIN_WEB_ROOT.'feedback.php'; ?>" id="feedback-nav">
    <i class="far fa-comment-dots"></i>
      <span>Feedback</span></a>
  </li>

  <hr class="sidebar-divider my-0">

  <li class="nav-item <?= basename($_SERVER['PHP_SELF']) == 'delete-user.php' ? 'active' : '' ; ?>">
    <a class="nav-link" href="<?= ADMIN_WEB_ROOT.'delete-user.php'; ?>" id="delete-user-nav">
    <i class="fas fa-user-times"></i>
      <span>Delete Users</span></a>
  </li>

  <hr class="sidebar-divider my-0">

  <li class="nav-item <?= basename($_SERVER['PHP_SELF']) == 'export-user.php' ? 'active' : '' ; ?>">
    <a class="nav-link" href="<?= ADMIN_WEB_ROOT.'export-user.php'; ?>" id="export-user-nav">
    <i class="fas fa-address-book"></i>
      <span>Export Users</span></a>
  </li>
 
  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

      <!-- Sidebar Toggle (Topbar) -->
      <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
      </button>

      <!-- Topbar Search -->
      <h3><?= ucfirst(basename($_SERVER['PHP_SELF'], ".php")); ?></h3>

      <!-- Topbar Navbar -->
      <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Alerts -->
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-bell fa-fw"></i>
            <!-- Counter - Alerts -->
            <span class="badge badge-danger badge-counter">3+</span>
          </a>
          <!-- Dropdown - Alerts -->
          <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
            <h6 class="dropdown-header">
              Alerts Center
            </h6>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="mr-3">
                <div class="icon-circle bg-primary">
                  <i class="fas fa-file-alt text-white"></i>
                </div>
              </div>
              <div>
                <div class="small text-gray-500">December 12, 2019</div>
                <span class="font-weight-bold">A new monthly report is ready to download!</span>
              </div>
            </a>
            <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
          </div>
        </li>

        <!-- Nav Item - Messages -->
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-envelope fa-fw"></i>
            <!-- Counter - Messages -->
            <span class="badge badge-danger badge-counter">7</span>
          </a>
          <!-- Dropdown - Messages -->
          <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
            <h6 class="dropdown-header">
              Message Center
            </h6>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="dropdown-list-image mr-3">
                <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
                <div class="status-indicator bg-success"></div>
              </div>
              <div class="font-weight-bold">
                <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
                <div class="small text-gray-500">Emily Fowler · 58m</div>
              </div>
            </a>
            <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
          </div>
        </li>

        <div class="topbar-divider d-none d-sm-block"></div>

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $currentAdmin['username'];?></span>
            <img class="img-profile rounded-circle" src="<?= IMG.'user-img/user.png' ; ?>">
          </a>
          <!-- Dropdown - User Information -->
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="<?= ADMIN_WEB_ROOT.'profile.php'; ?>">
              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
              Profile
            </a>
            <a class="dropdown-item" href="<?= ADMIN_WEB_ROOT.'setting.php'; ?>">
              <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
              Settings
            </a>
            <a class="dropdown-item" href="#">
              <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
              Activity Log
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
              Logout
            </a>
          </div>
        </li>

      </ul>

    </nav>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid" id="main-container">