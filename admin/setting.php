<?php

include_once realpath($_SERVER['DOCUMENT_ROOT'].'/user-management-system/bootstrap.php');

include (ADMIN.'model/session.php');

?>

<!DOCTYPE html>
<html lang="en">

<head>
<!-- css directory -->
<?php require_once(ROOT_DIR."/_inc/css.php");?>
<!-- Custom styles for this template-->
<link href="<?= CSS ;?>sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

<?php require_once( ADMIN.'admin_inc/header.php' ); ?>


<!-- ############Code here ############## -->


<?php require_once( ADMIN.'admin_inc/footer.php' ); ?>
  <!-- Bootstrap core JavaScript-->
<?php require_once(ROOT_DIR."/_inc/js.php");?>

<script>
$(document).ready( function () {

   
    
});
    
</script>


</body>

</html>
