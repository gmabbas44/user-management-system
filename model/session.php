<?php

use MyApp\Users\Users;

if (!isset($_SESSION['email'])) {
    header("location: ".WEB_ROOT."index.php");
    die();
}
$users = new Users();

$s_id = session_id();
$users->userOnline($s_id);

$currentUser = $users->loginUser($_SESSION['email']);
$cUserId = $currentUser['user_id'];

?>
